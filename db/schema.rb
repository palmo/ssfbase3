# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151203124131) do

  create_table "admin_users", force: :cascade do |t|
    t.string   "username",        limit: 255
    t.string   "password_digest", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "allenatores", force: :cascade do |t|
    t.string   "nome",            limit: 255
    t.string   "cognome",         limit: 255
    t.string   "telefono",        limit: 255
    t.string   "email",           limit: 255
    t.integer  "associazione_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "allenatores", ["associazione_id"], name: "index_allenatores_on_associazione_id", using: :btree

  create_table "associaziones", force: :cascade do |t|
    t.string   "ragione_sociale", limit: 255
    t.string   "indirizzo",       limit: 255
    t.string   "città",           limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "manifesto"
  end

  create_table "bambinos", force: :cascade do |t|
    t.string   "nome",                   limit: 255
    t.string   "cognome",                limit: 255
    t.date     "data_di_nascita"
    t.string   "luogo_di_nascita",       limit: 255
    t.string   "status",                 limit: 255
    t.string   "cittadinanza",           limit: 255
    t.string   "disciplina",             limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "tutor_id",               limit: 4
    t.integer  "associazione_id",        limit: 4
    t.integer  "allenatore_id",          limit: 4
    t.date     "data_inserimento"
    t.string   "situazione_abitativa",   limit: 255
    t.string   "nome_madre",             limit: 255
    t.string   "nome_padre",             limit: 255
    t.string   "cognome_padre",          limit: 255
    t.string   "cognome_madre",          limit: 255
    t.date     "fine_inserimento"
    t.integer  "coordinatore_id",        limit: 4
    t.string   "riferimento_telefonico", limit: 255
    t.string   "genere",                 limit: 255
    t.integer  "segnalatore_id",         limit: 4
  end

  add_index "bambinos", ["allenatore_id"], name: "index_bambinos_on_allenatore_id", using: :btree
  add_index "bambinos", ["coordinatore_id"], name: "index_bambinos_on_coordinatore_id", using: :btree
  add_index "bambinos", ["segnalatore_id"], name: "index_bambinos_on_segnalatore_id", using: :btree

  create_table "coordinatores", force: :cascade do |t|
    t.string   "nome",       limit: 255
    t.string   "cognome",    limit: 255
    t.string   "email",      limit: 255
    t.string   "telefono",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "disciplinas", force: :cascade do |t|
    t.string   "disciplina_sportiva", limit: 255
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "segnalatores", force: :cascade do |t|
    t.string   "ente",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "referente",  limit: 255
    t.string   "città",      limit: 255
    t.string   "telefono",   limit: 255
    t.string   "email",      limit: 255
  end

  create_table "tutors", force: :cascade do |t|
    t.string   "nome",       limit: 255
    t.string   "cognome",    limit: 255
    t.string   "email",      limit: 255
    t.string   "telefono",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_foreign_key "allenatores", "associaziones"
  add_foreign_key "bambinos", "allenatores"
  add_foreign_key "bambinos", "coordinatores"
  add_foreign_key "bambinos", "segnalatores"
end
