class RemoveTutorFromBambino < ActiveRecord::Migration
  def change
  	remove_column :bambinos, :tutor, :string
  	add_column :bambinos, :tutor_id, :integer
  end
end
