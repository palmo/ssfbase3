class AddDetailsToBambinos < ActiveRecord::Migration
  def change
    add_column :bambinos, :cognome_padre, :string
    add_column :bambinos, :cognome_madre, :string
  end
end
