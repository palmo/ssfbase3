class AddFineInserimentoToBambinos < ActiveRecord::Migration
  def change
    add_column :bambinos, :fine_inserimento, :date
  end
end
