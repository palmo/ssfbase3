class CreateSegnalatores < ActiveRecord::Migration
  def change
    create_table :segnalatores do |t|
      t.string :ente

      t.timestamps null: false
    end
  end
end
