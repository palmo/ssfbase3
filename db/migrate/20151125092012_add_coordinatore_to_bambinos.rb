class AddCoordinatoreToBambinos < ActiveRecord::Migration
  def change
  	
    add_reference :bambinos, :coordinatore, index: true, foreign_key: true
  
  end
end
