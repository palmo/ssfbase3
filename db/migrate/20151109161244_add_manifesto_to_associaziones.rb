class AddManifestoToAssociaziones < ActiveRecord::Migration
  def change
    add_column :associaziones, :manifesto, :boolean
  end
end
