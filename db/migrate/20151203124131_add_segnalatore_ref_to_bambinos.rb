class AddSegnalatoreRefToBambinos < ActiveRecord::Migration
  def change
    add_reference :bambinos, :segnalatore, index: true, foreign_key: true
  end
end
