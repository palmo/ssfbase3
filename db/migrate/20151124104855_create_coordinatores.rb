class CreateCoordinatores < ActiveRecord::Migration
  def change
    create_table :coordinatores do |t|
      t.string :nome
      t.string :cognome
      t.string :email
      t.string :telefono

      t.timestamps null: false
    end
  end
end
