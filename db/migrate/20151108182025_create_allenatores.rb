class CreateAllenatores < ActiveRecord::Migration
  def change
    create_table :allenatores do |t|
      t.string :nome
      t.string :cognome
      t.string :telefono
      t.string :email
      t.references :associazione, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
