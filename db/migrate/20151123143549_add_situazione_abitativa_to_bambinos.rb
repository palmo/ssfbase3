class AddSituazioneAbitativaToBambinos < ActiveRecord::Migration
  def change
    add_column :bambinos, :situazione_abitativa, :string
  end
end
