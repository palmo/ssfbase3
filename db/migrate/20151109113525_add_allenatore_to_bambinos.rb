class AddAllenatoreToBambinos < ActiveRecord::Migration
  def change
    add_reference :bambinos, :allenatore, index: true, foreign_key: true
  end
end
