class RemoveAssociazioneFromBambino < ActiveRecord::Migration
  def change
  	remove_column :bambinos, :associazione, :string
  	add_column :bambinos, :associazione_id, :integer

  end
end
