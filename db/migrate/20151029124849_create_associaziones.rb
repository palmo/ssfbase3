class CreateAssociaziones < ActiveRecord::Migration
  def change
    create_table :associaziones do |t|
      t.string :ragione_sociale
      t.string :indirizzo
      t.string :città

      t.timestamps null: false
    end
  end
end
