class AddDataInserimentoToBambinos < ActiveRecord::Migration
  def change
    add_column :bambinos, :data_inserimento, :date
  end
end
