class CreateBambinos < ActiveRecord::Migration
  def change
    create_table :bambinos do |t|
      t.string :nome
      t.string :cognome
      t.date :data_di_nascita
      t.string :luogo_di_nascita
      t.string :status
      t.string :cittadinanza
      t.string :disciplina
      t.string :associazione
      t.string :tutor

      t.timestamps null: false
    end
  end
end
