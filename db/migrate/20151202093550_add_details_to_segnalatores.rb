class AddDetailsToSegnalatores < ActiveRecord::Migration
  def change
    add_column :segnalatores, :referente, :string
    add_column :segnalatores, :città, :string
    add_column :segnalatores, :telefono, :string
    add_column :segnalatores, :email, :string
  end
end
