class CreateDisciplinas < ActiveRecord::Migration
  def change
    create_table :disciplinas do |t|
      t.string :disciplina_sportiva

      t.timestamps null: false
    end
  end
end
