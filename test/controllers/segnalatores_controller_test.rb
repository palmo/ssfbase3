require 'test_helper'

class SegnalatoresControllerTest < ActionController::TestCase
  setup do
    @segnalatore = segnalatores(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:segnalatores)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create segnalatore" do
    assert_difference('Segnalatore.count') do
      post :create, segnalatore: { ente: @segnalatore.ente }
    end

    assert_redirected_to segnalatore_path(assigns(:segnalatore))
  end

  test "should show segnalatore" do
    get :show, id: @segnalatore
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @segnalatore
    assert_response :success
  end

  test "should update segnalatore" do
    patch :update, id: @segnalatore, segnalatore: { ente: @segnalatore.ente }
    assert_redirected_to segnalatore_path(assigns(:segnalatore))
  end

  test "should destroy segnalatore" do
    assert_difference('Segnalatore.count', -1) do
      delete :destroy, id: @segnalatore
    end

    assert_redirected_to segnalatores_path
  end
end
