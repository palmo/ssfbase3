require 'test_helper'

class AllenatoresControllerTest < ActionController::TestCase
  setup do
    @allenatore = allenatores(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:allenatores)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create allenatore" do
    assert_difference('Allenatore.count') do
      post :create, allenatore: { associazione_id: @allenatore.associazione_id, cognome: @allenatore.cognome, email: @allenatore.email, nome: @allenatore.nome, telefono: @allenatore.telefono }
    end

    assert_redirected_to allenatore_path(assigns(:allenatore))
  end

  test "should show allenatore" do
    get :show, id: @allenatore
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @allenatore
    assert_response :success
  end

  test "should update allenatore" do
    patch :update, id: @allenatore, allenatore: { associazione_id: @allenatore.associazione_id, cognome: @allenatore.cognome, email: @allenatore.email, nome: @allenatore.nome, telefono: @allenatore.telefono }
    assert_redirected_to allenatore_path(assigns(:allenatore))
  end

  test "should destroy allenatore" do
    assert_difference('Allenatore.count', -1) do
      delete :destroy, id: @allenatore
    end

    assert_redirected_to allenatores_path
  end
end
