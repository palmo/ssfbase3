require 'test_helper'

class BambinosControllerTest < ActionController::TestCase
  setup do
    @bambino = bambinos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bambinos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bambino" do
    assert_difference('Bambino.count') do
      post :create, bambino: { associazione: @bambino.associazione, cittadinanza: @bambino.cittadinanza, cognome: @bambino.cognome, data_di_nascita: @bambino.data_di_nascita, disciplina: @bambino.disciplina, luogo_di_nascita: @bambino.luogo_di_nascita, nome: @bambino.nome, status: @bambino.status, tutor: @bambino.tutor }
    end

    assert_redirected_to bambino_path(assigns(:bambino))
  end

  test "should show bambino" do
    get :show, id: @bambino
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bambino
    assert_response :success
  end

  test "should update bambino" do
    patch :update, id: @bambino, bambino: { associazione: @bambino.associazione, cittadinanza: @bambino.cittadinanza, cognome: @bambino.cognome, data_di_nascita: @bambino.data_di_nascita, disciplina: @bambino.disciplina, luogo_di_nascita: @bambino.luogo_di_nascita, nome: @bambino.nome, status: @bambino.status, tutor: @bambino.tutor }
    assert_redirected_to bambino_path(assigns(:bambino))
  end

  test "should destroy bambino" do
    assert_difference('Bambino.count', -1) do
      delete :destroy, id: @bambino
    end

    assert_redirected_to bambinos_path
  end
end
