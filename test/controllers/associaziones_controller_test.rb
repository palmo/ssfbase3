require 'test_helper'

class AssociazionesControllerTest < ActionController::TestCase
  setup do
    @associazione = associaziones(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:associaziones)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create associazione" do
    assert_difference('Associazione.count') do
      post :create, associazione: { città: @associazione.città, indirizzo: @associazione.indirizzo, ragione_sociale: @associazione.ragione_sociale }
    end

    assert_redirected_to associazione_path(assigns(:associazione))
  end

  test "should show associazione" do
    get :show, id: @associazione
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @associazione
    assert_response :success
  end

  test "should update associazione" do
    patch :update, id: @associazione, associazione: { città: @associazione.città, indirizzo: @associazione.indirizzo, ragione_sociale: @associazione.ragione_sociale }
    assert_redirected_to associazione_path(assigns(:associazione))
  end

  test "should destroy associazione" do
    assert_difference('Associazione.count', -1) do
      delete :destroy, id: @associazione
    end

    assert_redirected_to associaziones_path
  end
end
