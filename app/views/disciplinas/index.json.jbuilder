json.array!(@disciplinas) do |disciplina|
  json.extract! disciplina, :id, :disciplina_sportiva
  json.url disciplina_url(disciplina, format: :json)
end
