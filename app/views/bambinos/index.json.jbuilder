json.array!(@bambinos) do |bambino|
  json.extract! bambino, :id, :nome, :cognome, :data_di_nascita, :luogo_di_nascita, :status, :cittadinanza, :disciplina, :associazione, :tutor
  json.url bambino_url(bambino, format: :json)
end
