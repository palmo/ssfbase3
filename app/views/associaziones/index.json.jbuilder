json.array!(@associaziones) do |associazione|
  json.extract! associazione, :id, :ragione_sociale, :indirizzo, :città
  json.url associazione_url(associazione, format: :json)
end
