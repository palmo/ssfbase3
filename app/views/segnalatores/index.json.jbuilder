json.array!(@segnalatores) do |segnalatore|
  json.extract! segnalatore, :id, :ente
  json.url segnalatore_url(segnalatore, format: :json)
end
