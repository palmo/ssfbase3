json.array!(@allenatores) do |allenatore|
  json.extract! allenatore, :id, :nome, :cognome, :telefono, :email, :associazione_id
  json.url allenatore_url(allenatore, format: :json)
end
