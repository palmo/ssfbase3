class Segnalatore < ActiveRecord::Base

	has_many :bambinos
	
	validates_presence_of :ente
	
	
	def città=(value)
  	write_attribute(:città, value.capitalize)
	end

end
