class Disciplina < ActiveRecord::Base

	validates_presence_of :disciplina_sportiva

	def disciplina_sportiva=(value)

		write_attribute(:disciplina_sportiva, value.downcase)

	end
end
