class Bambino < ActiveRecord::Base
    
    belongs_to :tutor
    belongs_to :associazione
    belongs_to :allenatore
    belongs_to :coordinatore
    belongs_to :segnalatore

    validates_presence_of  :nome, :cognome

	def nome=(value)
  	write_attribute(:nome, value.capitalize)
	end

	def cognome=(value)
  	write_attribute(:cognome, value.capitalize)
	end

	def cittadinanza=(value)
  	write_attribute(:cittadinanza, value.capitalize)
	end
	def disciplina=(value)
  	write_attribute(:disciplina, value.downcase)
	end
end
