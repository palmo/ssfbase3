class Associazione < ActiveRecord::Base

	has_many :bambinos
	has_many :allenatores

	validates_presence_of :ragione_sociale
	
	
	def città=(value)
  	write_attribute(:città, value.capitalize)
	end

end
