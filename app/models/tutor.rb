class Tutor < ActiveRecord::Base

	has_many :bambinos

	validates_presence_of  :nome, :cognome

	def nome=(value)
  	write_attribute(:nome, value.capitalize)
	end

	def cognome=(value)
  	write_attribute(:cognome, value.capitalize)
	end
end
