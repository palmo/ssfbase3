class Allenatore < ActiveRecord::Base
  belongs_to :associazione
  has_many :bambinos

  validates_presence_of :nome,:cognome,:associazione_id

  def nome=(value)
  	write_attribute(:nome,value.capitalize)
  end
  def cognome=(value)
  	write_attribute(:cognome,value.capitalize)
  end

end
