class AllenatoresController < ApplicationController
  before_action :confirm_logged_in
  before_action :set_allenatore, only: [:show, :edit, :update, :destroy]

  # GET /allenatores
  # GET /allenatores.json
  def index
    @allenatores = Allenatore.all
  end

  # GET /allenatores/1
  # GET /allenatores/1.json
  def show
  end

  # GET /allenatores/new
  def new
    @allenatore = Allenatore.new
  end

  # GET /allenatores/1/edit
  def edit
  end

  # POST /allenatores
  # POST /allenatores.json
  def create
    @allenatore = Allenatore.new(allenatore_params)

    respond_to do |format|
      if @allenatore.save
        format.html { redirect_to @allenatore, notice: 'Allenatore was successfully created.' }
        format.json { render :show, status: :created, location: @allenatore }
      else
        format.html { render :new }
        format.json { render json: @allenatore.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /allenatores/1
  # PATCH/PUT /allenatores/1.json
  def update
    respond_to do |format|
      if @allenatore.update(allenatore_params)
        format.html { redirect_to @allenatore, notice: 'Allenatore was successfully updated.' }
        format.json { render :show, status: :ok, location: @allenatore }
      else
        format.html { render :edit }
        format.json { render json: @allenatore.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /allenatores/1
  # DELETE /allenatores/1.json
  def destroy
    @allenatore.destroy
    respond_to do |format|
      format.html { redirect_to allenatores_url, notice: 'Allenatore was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_allenatore
      @allenatore = Allenatore.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def allenatore_params
      params.require(:allenatore).permit(:nome, :cognome, :telefono, :email, :associazione_id)
    end
end
