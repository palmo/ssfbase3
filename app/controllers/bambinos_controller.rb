class BambinosController < ApplicationController
  before_action :confirm_logged_in
  before_action  :set_bambino, only: [:show, :edit, :update, :destroy]

  # GET /bambinos
  # GET /bambinos.json
  def index
    @bambinos = Bambino.all
  end

  # GET /bambinos/1
  # GET /bambinos/1.json
  def show
  end

  # GET /bambinos/new
  def new
    @bambino = Bambino.new
  end

  # GET /bambinos/1/edit
  def edit
  end

  # POST /bambinos
  # POST /bambinos.json
  def create
    @bambino = Bambino.new(bambino_params)

    respond_to do |format|
      if @bambino.save
        format.html { redirect_to @bambino, notice: 'Bambino was successfully created.' }
        format.json { render :show, status: :created, location: @bambino }
      else
        format.html { render :new }
        format.json { render json: @bambino.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bambinos/1
  # PATCH/PUT /bambinos/1.json
  def update
    respond_to do |format|
      if @bambino.update(bambino_params)
        format.html { redirect_to @bambino, notice: 'Bambino was successfully updated.' }
        format.json { render :show, status: :ok, location: @bambino }
      else
        format.html { render :edit }
        format.json { render json: @bambino.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bambinos/1
  # DELETE /bambinos/1.json
  def destroy
    @bambino.destroy
    respond_to do |format|
      format.html { redirect_to bambinos_url, notice: 'Bambino was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bambino
      @bambino = Bambino.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bambino_params
      params.require(:bambino).permit(:nome, 
        :cognome, 
        :data_di_nascita, 
        :luogo_di_nascita,
        :genere, 
        :status, 
        :cittadinanza, 
        :disciplina, 
        :associazione_id, 
        :tutor_id, 
        :allenatore_id, 
        :data_inserimento,
        :situazione_abitativa,
        :nome_madre,
        :cognome_madre,
        :nome_padre,
        :cognome_padre,
        :fine_inserimento,
        :coordinatore_id,
        :riferimento_telefonico,
        :segnalatore_id)
    end
end
