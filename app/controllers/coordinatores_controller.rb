class CoordinatoresController < ApplicationController
  before_action :confirm_logged_in
  before_action :set_coordinatore, only: [:show, :edit, :update, :destroy]

  # GET /tutors
  # GET /tutors.json
  def index
    @coordinatores = Coordinatore.all
  end

  # GET /tutors/1
  # GET /tutors/1.json
  def show
  end

  # GET /tutors/new
  def new
    @coordinatore = Coordinatore.new
  end

  # GET /tutors/1/edit
  def edit
  end

  # POST /tutors
  # POST /tutors.json
  def create
    @coordinatore = Coordinatore.new(coordinatore_params)

    respond_to do |format|
      if @coordinatore.save
        format.html { redirect_to @coordinatore, notice: 'Coordinatore was successfully created.' }
        format.json { render :show, status: :created, location: @coordinatore }
      else
        format.html { render :new }
        format.json { render json: @coordinatore.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tutors/1
  # PATCH/PUT /tutors/1.json
  def update
    respond_to do |format|
      if @coordinatore.update(coordinatore_params)
        format.html { redirect_to @coordinatore, notice: 'Coordinatore was successfully updated.' }
        format.json { render :show, status: :ok, location: @coordinatore }
      else
        format.html { render :edit }
        format.json { render json: @coordinatore.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tutors/1
  # DELETE /tutors/1.json
  def destroy
    @coordinatore.destroy
    respond_to do |format|
      format.html { redirect_to coordinatores_url, notice: 'Coordinatore was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coordinatore
      @coordinatore = Coordinatore.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def coordinatore_params
      params.require(:coordinatore).permit(:nome, :cognome, :email, :telefono)
    end
end
