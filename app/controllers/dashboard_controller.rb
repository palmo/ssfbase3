class DashboardController < ApplicationController
before_action :confirm_logged_in
	def index
		@discipline = Disciplina.count
		@bambini = Bambino.count
		@tutors = Tutor.count
		@nazionalità = Bambino.distinct.count(:cittadinanza)
		@inseriti = Bambino.where(status: 'inserito').count
		@segnalati = Bambino.where(status: 'segnalato').count
		@drop = Bambino.where(status: 'drop').count
		@allenatori = Allenatore.distinct.count
		@coordinatori = Coordinatore.distinct.count
		@senzatutor = Bambino.where(tutor_id: 10).count
		@bambiniMichele = Bambino.where(coordinatore_id: 1).count
		@bambiniFederico = Bambino.where(coordinatore_id: 2).count
		@bambiniGeralda = Bambino.where(coordinatore_id: 3).count
		@bambiniCarola = Bambino.where(coordinatore_id: 4).count
		@bambiniErika = Bambino.where(coordinatore_id: 5).count
		@bambiniValeria = Bambino.where(coordinatore_id: 6).count
		@associazioni = Associazione.count
		@segnalatori = Segnalatore.count
	end
end
