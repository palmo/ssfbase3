class AssociazionesController < ApplicationController
  before_action :confirm_logged_in
  before_action :set_associazione, only: [:show, :edit, :update, :destroy]

  # GET /associaziones
  # GET /associaziones.json
  def index
    @associaziones = Associazione.all
  end

  # GET /associaziones/1
  # GET /associaziones/1.json
  def show
  end

  # GET /associaziones/new
  def new
    @associazione = Associazione.new
  end

  # GET /associaziones/1/edit
  def edit
  end

  # POST /associaziones
  # POST /associaziones.json
  def create
    @associazione = Associazione.new(associazione_params)

    respond_to do |format|
      if @associazione.save
        format.html { redirect_to @associazione, notice: 'Associazione was successfully created.' }
        format.json { render :show, status: :created, location: @associazione }
      else
        format.html { render :new }
        format.json { render json: @associazione.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /associaziones/1
  # PATCH/PUT /associaziones/1.json
  def update
    respond_to do |format|
      if @associazione.update(associazione_params)
        format.html { redirect_to @associazione, notice: 'Associazione was successfully updated.' }
        format.json { render :show, status: :ok, location: @associazione }
      else
        format.html { render :edit }
        format.json { render json: @associazione.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /associaziones/1
  # DELETE /associaziones/1.json
  def destroy
    @associazione.destroy
    respond_to do |format|
      format.html { redirect_to associaziones_url, notice: 'Associazione was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_associazione
      @associazione = Associazione.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def associazione_params
      params.require(:associazione).permit(:ragione_sociale, :indirizzo, :città, :manifesto)
    end
end
