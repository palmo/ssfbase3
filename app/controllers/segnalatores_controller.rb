class SegnalatoresController < ApplicationController
  before_action :confirm_logged_in
  before_action :set_segnalatore, only: [:show, :edit, :update, :destroy]

  # GET /segnalatores
  # GET /segnalatores.json
  def index
    @segnalatores = Segnalatore.all
  end

  # GET /segnalatores/1
  # GET /segnalatores/1.json
  def show
  end

  # GET /segnalatores/new
  def new
    @segnalatore = Segnalatore.new
  end

  # GET /segnalatores/1/edit
  def edit
  end

  # POST /segnalatores
  # POST /segnalatores.json
  def create
    @segnalatore = Segnalatore.new(segnalatore_params)

    respond_to do |format|
      if @segnalatore.save
        format.html { redirect_to @segnalatore, notice: 'Segnalatore was successfully created.' }
        format.json { render :show, status: :created, location: @segnalatore }
      else
        format.html { render :new }
        format.json { render json: @segnalatore.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /segnalatores/1
  # PATCH/PUT /segnalatores/1.json
  def update
    respond_to do |format|
      if @segnalatore.update(segnalatore_params)
        format.html { redirect_to @segnalatore, notice: 'Segnalatore was successfully updated.' }
        format.json { render :show, status: :ok, location: @segnalatore }
      else
        format.html { render :edit }
        format.json { render json: @segnalatore.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /segnalatores/1
  # DELETE /segnalatores/1.json
  def destroy
    @segnalatore.destroy
    respond_to do |format|
      format.html { redirect_to segnalatores_url, notice: 'Segnalatore was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_segnalatore
      @segnalatore = Segnalatore.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def segnalatore_params
      params.require(:segnalatore).permit(:ente,
        :referente,
        :città,
        :telefono,
        :email)
    end
end
